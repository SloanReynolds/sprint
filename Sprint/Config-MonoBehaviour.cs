﻿using System.IO;
using UnityEngine;

namespace Sprint {
	class Config : MonoBehaviour {
		private const string CONFIG_PATH = @".\QMods\Sprint\config.txt";

		#region ConfigDefaults
		internal bool ToggleMode {
				get {
					DeferredLoad();
					return pToggleMode;
				}
			}
			private bool pToggleMode = true;

			internal float EnergyCost {
				get {
					DeferredLoad();
					return pEnergyCost;
				}
			}
			private float pEnergyCost = 0.04f;

			internal float Speed {
				get {
					DeferredLoad();
					return pSpeed;
				}
			}
			private float pSpeed = 7.0f;

			internal float WalkSpeed {
				get {
					DeferredLoad();
					return pWalkSpeed;
				}
			}
			private float pWalkSpeed = LazyConsts.PLAYER_SPEED;
		#endregion

		internal bool IsKeyPressed {
			get {
				return Input.GetKeyDown(KeyCode.LeftShift) || LazyInput.GetKeyDown(GameKey.NextSubTub);
			}
		}

		internal bool IsKeyHeld {
			get {
				return Input.GetKey(KeyCode.LeftShift) || LazyInput.GetKey(GameKey.NextSubTub);
			}
		}

		internal bool ShouldSprint => (ToggleMode && IsSprinting) || IsKeyHeld;

		internal bool IsSprinting {
			get; private set;
		}

		private bool init = false;

		private void DeferredLoad() {
			if (!init) {
				WriteConfigFile();

				foreach (string line in File.ReadLines(CONFIG_PATH)) {
					if (line.Trim() != "") {
						string[] opt = line.Split('=');
						switch (opt[0]) {
							case "setToggleMode":
								if (bool.TryParse(opt[1], out bool toggleMode))
									pToggleMode = toggleMode;
								break;
							case "energyCost":
								if (float.TryParse(opt[1], out float energyCost))
									pEnergyCost = energyCost;
								break;
							case "speed":
								if (float.TryParse(opt[1], out float speed))
									pSpeed = speed;
								break;
							case "walkSpeed":
								if (float.TryParse(opt[1], out float walkSpeed))
									pWalkSpeed = walkSpeed;
								break;
							default:
								// Invalid Option in config file?
								break;
						}
					}
				}

				init = true;
			}
		}

		public static void Log(string v) {
#if DEBUG
			using (StreamWriter sw = File.AppendText(@".\QMods\Sprint\log.txt")) {
				sw.WriteLine(v);
			}
#endif
		}

		private void WriteConfigFile() {
			if (!File.Exists(CONFIG_PATH)) {
				using (StreamWriter sw = File.CreateText(CONFIG_PATH)) {
					sw.WriteLine($"setToggleMode={pToggleMode}");
					sw.WriteLine($"energyCost={pEnergyCost}");
					sw.WriteLine($"speed={pSpeed}");
					sw.WriteLine($"walkSpeed={pSpeed}");
				}
			}
		}

		internal void ToggleSprinting() {
			SetSprinting(!IsSprinting);
		}

		internal void SetSprinting(bool spr) {
			IsSprinting = spr;
		}
	}
}
