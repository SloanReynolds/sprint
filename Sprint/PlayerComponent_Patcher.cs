﻿using Harmony;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Sprint {
	[HarmonyPatch(typeof(PlayerComponent))]
	[HarmonyPatch("Update")]
	internal class PlayerComponent_Update_Patch {
		[HarmonyPrefix]
		public static void Prefix(PlayerComponent __instance) {
			if (__instance.wgo.is_player) {
				Config config = SingletonGameObjects.FindOrCreate<Config>();
				WorldGameObject wgo = __instance.wgo;
				MovementComponent mc = wgo.components.character;

				float moveSpeed = config.WalkSpeed; //Original Speed

				if (config.ToggleMode && config.IsKeyPressed) {
					config.ToggleSprinting();
				}

				if (config.ShouldSprint) {
					if (__instance.TrySpendEnergy(config.EnergyCost)) {
						moveSpeed = config.Speed; //Faster now!~
					} else if (config.ToggleMode && config.IsSprinting) {
						config.SetSprinting(false);
					}
				}

				//We need to apply the Speed Buff now since the game will do it if an only if our original speed == LazyConsts.PLAYER_SPEED exactly.
				if (moveSpeed != LazyConsts.PLAYER_SPEED) {
					//Apply the buff, if one exists.
					moveSpeed += wgo.data.GetParam("speed_buff"); //Defaults to 0f
					if (moveSpeed == LazyConsts.PLAYER_SPEED) {
						//Avoid the double-buff
						moveSpeed += 0.1f;
					}
				}

				mc.SetSpeed(moveSpeed);
			}
		}
	}
}